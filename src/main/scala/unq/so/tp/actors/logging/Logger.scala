package unq.so.tp.actors.logging

import akka.actor._
import unq.so.tp.actors.SOActor
import unq.so.tp.actors.fsmpersistence.{SOState, UninitializedData, UninitializedState, Data}
import unq.so.tp.actors.sistema.{IIM, IRQ}
import akka.util.Timeout
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.Await
import unq.so.tp.actors.sistema.memory.Translate

class Logger extends Actor with FSM[SOState, Data] {
  startWith(DebugLevel, UninitializedData)

  when(DebugLevel){
    case Event(OutputOnly, data) =>
      goto(OutputOnly)
    case Event(Output(texto: String), data) =>
      println(texto)
      stay()
    case Event(texto:String, data) =>
      println(texto)
      stay()
    case _ => stay()
  }

  when(OutputOnly){
    case Event(DebugLevel, data) =>
      goto(DebugLevel)
    case Event(Output(texto: String), data) =>
      println(texto)
      stay()
    case _ => stay()
  }
}

case object DebugLevel extends SOState
case object OutputOnly extends SOState

case class Output(content:String)

object Logger {
  val LOGGER_ID = "logger"
}

case class LogActor(ref : ActorRef){
  def !(param: Any){
    ref ! param
  }
}

trait Loggable {
  implicit protected val context : ActorContext

  implicit val logger: LogActor = {
    val timeout: FiniteDuration = new FiniteDuration(2, java.util.concurrent.TimeUnit.SECONDS)
    LogActor(Await.result(context.actorSelection(s"akka://default/user/${Logger.LOGGER_ID}").resolveOne(timeout), timeout))
  }

}
