package unq.so.tp.actors.terminal

class TerminalCommands {

}

sealed trait TerminalCommand
case class StartTerminal() extends TerminalCommand
case class TerminalInput(command: Array[String]) extends TerminalCommand
case class LoadKernel(configFile: String) extends TerminalCommand