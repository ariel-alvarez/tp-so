package unq.so.tp.actors.terminal

import akka.actor._
import akka.pattern.ask
import unq.so.tp.actors.fsmpersistence.{Data, SOState}
import unq.so.tp.actors.logging._
import unq.so.tp.actors.sistema.{StartUp, _}
import unq.so.tp.actors.sistema.filesystem.ReadFile
import unq.so.tp.actors.soframework.FiveTimeout
import unq.so.tp.actors.SOActor
import unq.so.tp.actors.terminal.StartTerminal
import unq.so.tp.actors.terminal.TerminalInput
import unq.so.tp.actors.sistema.StartUp
import unq.so.tp.actors.sistema.SysCall
import unq.so.tp.actors.logging.Output
import akka.actor.InvalidActorNameException
import unq.so.tp.actors.terminal.TerminalData
import unq.so.tp.actors.sistema.Exec
import unq.so.tp.actors.sistema.File
import unq.so.tp.actors.sistema.filesystem.ReadFile

class TerminalDispatcher extends SOActor with FiveTimeout {

  startWith(SystemUp, TerminalData(List[ActorRef]()))

  val terminal = context.actorOf(Props[TerminalActor], "terminal")

  when(SystemUp) {

    case Event(StartTerminal, TerminalData(receivers)) =>
      terminal ! StartTerminal()
      stay using TerminalData( receivers )

    case Event(TerminalInput(comando), TerminalData(receivers)) => comando match {
      case Array("startup", "kernel",configFile) =>
        try {
          val booter = context.actorOf(Props[Booter], "booter")
          booter ! StartUp(configFile)
          stay using TerminalData( booter :: receivers )
        }
        catch {
          case InvalidActorNameException(message) => logger ! "El sistema ya se encuentra corriendo"

            stay using TerminalData(receivers )
        }

      case Array("cat", filename) =>
        val filesystem = context.actorSelection("akka://default/user/manager/booter/filesystem-*")

        filesystem.resolveOne().onSuccess {
          case fs : ActorRef ⇒ (fs ? SysCall(ReadFile("/", filename))) onSuccess {
            case File(_,_, lineas) => lineas.foreach( l => logger ! l)
          }
        }

        stay using TerminalData(receivers)

      case Array("exec", fileName) =>
        findKernel() ! SysCall(Exec("/", fileName, 9999))
        stay using TerminalData(receivers)

      case Array("exec", fileName, priority) =>
        findKernel() ! SysCall(Exec("/", fileName, priority.toInt))
        stay using TerminalData(receivers)

      case Array("state", device) =>
        context.actorSelection(s"akka://default/user/manager/booter/*$device") ! DescribeState
        stay using TerminalData(receivers)

      case Array("loglevel", "debug") =>
        logger ! DebugLevel
        logger ! "Logger changed to debug level"
        stay using TerminalData(receivers)

      case Array("loglevel", "output") =>
        logger ! OutputOnly
        logger ! Output("Logger changed to output level")
        stay using TerminalData(receivers)

      case _ =>
        println(
          """
            |Comando inválido.
            |Ejecutar 'help' para conocer la lista de comandos.
          """.stripMargin)
        stay using TerminalData( receivers )
    }
  }
}

// States of FSM
case object SystemUp extends SOState

// Data that may be retained within FSM
case object Uninitialized extends Data
case class TerminalData(targets: List[ActorRef]) extends Data