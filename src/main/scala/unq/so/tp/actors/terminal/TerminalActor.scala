package unq.so.tp.actors.terminal

import akka.actor.Actor
import akka.event.Logging

class TerminalActor extends Actor {
  val log = Logging(context.system, this)

  def receive = {
    case message: TerminalCommand => message match {
      case StartTerminal() =>
        log.debug("EnableConsole received")
        acceptUserInput()
      case _ =>
    }
    case _ =>
  }

  def acceptUserInput() = {
    println(
      """

        |Bienvenido a la consola de <Completar nombre del SO>!
        |startup kernel <configuracion> # Ej: Config1
        |cat <archivo> #muestra el contenido de un archivo
        |exec <programa> #ejecuta el contenido de un archivo

      """.stripMargin)

    for (ln <- io.Source.stdin.getLines().takeWhile(!_.equals("quit"))) {
      context.parent ! TerminalInput(ln.split(" "))
    }
  }
}
