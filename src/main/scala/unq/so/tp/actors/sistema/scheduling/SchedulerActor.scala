package unq.so.tp.actors.sistema.scheduling

import akka.actor.{FSM, ActorRef, ActorSystem}
import unq.so.tp.actors.dispositivos.RunPCB
import unq.so.tp.actors.fsmpersistence._
import unq.so.tp.actors.SOActor
import unq.so.tp.actors.sistema._
import unq.so.tp.actors.sistema.scheduling.algorithms.SchedulingAlgorithm
import unq.so.tp.actors.sistema.IIM
import unq.so.tp.actors.sistema.scheduling.CPUInitialized
import unq.so.tp.actors.sistema.PCB
import unq.so.tp.actors.sistema.scheduling.ProcessReady
import unq.so.tp.actors.sistema.scheduling.CPUFree
import unq.so.tp.actors.sistema.IRQ
import unq.so.tp.actors.sistema.scheduling.Scheduling
import unq.so.tp.actors.sistema.scheduling.ProcessWrapper
import unq.so.tp.actors.dispositivos.RunPCB
import unq.so.tp.actors.sistema.scheduling.IOFinished
import unq.so.tp.actors.sistema.memory.Free

case object SchedulingConstants {
  val NO_TIMEOUT = -1
}

case class SchedulerActor(schedulingAlgorithms: Seq[SchedulingAlgorithm]) extends SOActor {
  val system = ActorSystem("system")

  startWith(UninitializedState, Scheduling(List.empty[ProcessWrapper], List.empty[ActorRef]))

  when(UninitializedState) {

    case Event(IIM(m), data) => m match {

      case Initialize =>
        findCPUs() ! IIM(Initialize)
        goto(RunningState) using Scheduling(List.empty[ProcessWrapper], List.empty[ActorRef])

    }

    case Event(e, unEstado) =>
      logger ! s"${this.self.path} recibió ${e.toString} estando en ${unEstado.toString}"
      stay using unEstado
  }

  when(RunningState) {
    case Event(IRQ(e), Scheduling(aReadyList, cpus)) => e match {

      case ProcessReady(pcb, priority) =>
        val fold: ProcessWrapper = schedulingAlgorithms
          .foldLeft(ProcessWrapper(pcb, SchedulingConstants.NO_TIMEOUT, 0, priority))((acum: ProcessWrapper, algor: SchedulingAlgorithm) => algor.initialize(acum))
        runOrSchedule(aReadyList, fold, cpus)

      case IOFinished(schedulingStructure) =>
        logger ! s"SCHEDULER: IO FINISHED -> ${schedulingStructure.pcb}"
        runOrSchedule(aReadyList, schedulingStructure, cpus)

      case TimedOut(wrapper, cpuRef) =>
        logger ! s"SCHEDULER: TIMED OUT PROCESS -> ${wrapper.pcb}"
        runOrSchedule(
          aReadyList,
          ProcessWrapper(
              wrapper.pcb,
              wrapper.timeout,
              if (wrapper.remainingCycles == 0) wrapper.timeout else wrapper.remainingCycles,
              wrapper.priority
          ),
          cpus :+ cpuRef
        )

      case CPUFree(cpuRef) =>
        logger ! s"SCHEDULER: CPU FREE $cpuRef"
        if (!aReadyList.isEmpty) {
          sendNextProcessForExecution(cpuRef, aReadyList, cpus)
        } else {
          stay using Scheduling(aReadyList, cpus :+ cpuRef)
        }

      case ProcessFinished(cpuRef, wrapper) =>
        logger ! s"SCHEDULER: PROCESS FINISHED ${wrapper.pcb}"
        logger ! s"SCHEDULER: CPU FREE $cpuRef"
        findMemStrategy() ! IIM(Free(wrapper.pcb))
        if (!aReadyList.isEmpty) {
          sendNextProcessForExecution(cpuRef, aReadyList, cpus)
        } else {
          stay using Scheduling(aReadyList, cpus :+ cpuRef)
        }

    }

    case Event(IIM(m), Scheduling(aReadyList, cpus)) => m match {
      case CPUInitialized(cpuRef: ActorRef) =>
        stay using Scheduling(aReadyList, cpus :+ cpuRef)
    }
  }

  def sendNextProcessForExecution(cpuRef: ActorRef, aReadyList: List[ProcessWrapper], cpus: List[ActorRef]): FSM.State[SOState, Data] = {
    cpuRef ! IRQ(RunPCB(aReadyList.head))
    stay using Scheduling(aReadyList.tail, cpus :+ cpuRef)
  }

  def runOrSchedule(pcbList: List[ProcessWrapper], incoming: ProcessWrapper, cpus: List[ActorRef]): FSM.State[SOState, Data] = {
    logger ! s"SCHEDULER:  added to ready ${incoming.pcb.toString}"
    val wrappers: List[ProcessWrapper] = applySchedulingStrategy(pcbList, incoming)

    if (!cpus.isEmpty) {
      logger ! s"SCHEDULER: send for execution ${wrappers.head} on ${cpus.head.toString()}"
      cpus.head ! IRQ(RunPCB(wrappers.head))
      stay using Scheduling(wrappers.tail, cpus.tail)

    } else stay using Scheduling(wrappers, cpus)

  }

  def applySchedulingStrategy(pcbList: List[ProcessWrapper], incoming: ProcessWrapper): List[ProcessWrapper] = {
      schedulingAlgorithms
        .foldLeft(pcbList :+ incoming)((acum: List[ProcessWrapper], algor: SchedulingAlgorithm) => algor.schedule(acum))
  }
}

// States of FSM
case object RunningState extends SOState

// Data that may be retained within FSM
case object Idle extends Data

case class Scheduling(readyProcess: List[ProcessWrapper], availableCpus: List[ActorRef]) extends Data

case class ProcessWrapper(pcb: PCB, timeout: Int, remainingCycles: Int, priority: Int)

case class ProcessReady(pcb: PCB, priority:Int) extends InterruptionType

case class IOFinished(data: ProcessWrapper) extends InterruptionType

case class TimedOut(data: ProcessWrapper, cpu: ActorRef) extends InterruptionType

case class CPUFree(ref: ActorRef) extends InterruptionType

case class ProcessFinished(ref: ActorRef, wrapper: ProcessWrapper) extends InterruptionType

//Internal Messages
case class CPUInitialized(ref: ActorRef) extends InternalImplementationMessageType