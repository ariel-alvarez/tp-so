package unq.so.tp.actors.sistema

import akka.actor.{Actor, ActorRef, FSM, Props}
import akka.pattern.ask
import akka.util.Timeout
import unq.so.tp.actors.config.{Configuration, ConfigurationManager, FindConfiguration}
import unq.so.tp.actors.fsmpersistence.{Data, SOState, UninitializedData}
import unq.so.tp.actors.logging.Loggable

import scala.concurrent.Await
import scala.concurrent.duration._

class Booter extends Actor with FSM[SOState, Data] with Loggable{

  implicit val timeout = Timeout(5 seconds)
  startWith(NotBooted, UninitializedData)

  when(NotBooted) {
    case Event(StartUp(configurationName), UninitializedData) =>

      val configurationManager: ActorRef = context.actorOf(Props[ConfigurationManager], "ConfigurationManager")
      Await.result(configurationManager ? FindConfiguration(configurationName), timeout.duration)
        .asInstanceOf[Option[Configuration]] match {
        case Some(config:Configuration) =>
          config.apply(context)
          goto(Booted) using BooterConfig(config)
        case None =>
          logger ! s"La configuración $configurationName no existe"
          stop()
      }

  }

  when(Booted) {
    case Event(_, data) =>
      stay using data
  }
}

//Estados
case object Booted extends SOState
case object NotBooted extends SOState

//Estructura
case class BooterConfig(c:Configuration) extends Data

//Mensajes
case class StartUp(configId:String)