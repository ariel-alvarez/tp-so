package unq.so.tp.actors.sistema.scheduling.algorithms

import unq.so.tp.actors.sistema.scheduling.ProcessWrapper

trait SchedulingAlgorithm {
  def schedule(current : List[ProcessWrapper]): List[ProcessWrapper]
  def initialize(incoming: ProcessWrapper):ProcessWrapper
}
