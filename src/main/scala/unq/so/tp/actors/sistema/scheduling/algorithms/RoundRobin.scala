package unq.so.tp.actors.sistema.scheduling.algorithms

import unq.so.tp.actors.sistema.scheduling.ProcessWrapper

case class RoundRobin(timeout:Int) extends SchedulingAlgorithm {
  override def schedule(current: List[ProcessWrapper]): List[ProcessWrapper] = current

  override def initialize(incoming: ProcessWrapper): ProcessWrapper = ProcessWrapper(incoming.pcb, timeout, timeout, incoming.priority)
}
