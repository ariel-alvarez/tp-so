package unq.so.tp.actors.sistema.filesystem
import akka.actor.{Actor, FSM}
import unq.so.tp.actors.fsmpersistence._
import unq.so.tp.actors.logging.Loggable
import unq.so.tp.actors.sistema.{File, IIM, IRQ, SysCall, _}

import scala.io.Source
import unq.so.tp.actors.SOActor
import java.io.FileNotFoundException

class TrivialFilesystemActor(basePath:String) extends SOActor {

  startWith(UninitializedState, UninitializedData)

  when(UninitializedState) {
    case Event(IIM(internalMessage),_) => internalMessage match {
      case Initialize =>
        logger ! s"Creado filesystem en ${context.self.path}"
        goto(InitializedState) using TrivialFileSystemData(basePath)
    }

  }

  when(InitializedState) {
    case Event(event, previousState:TrivialFileSystemData) =>
      event match {

        //Mensajes del SO
        case e: IRQ => stay using previousState

        //Mensajes de la implementación
        case IIM(m) => m match {
          case InitPath(path:String) =>
            logger ! s"Creado filesystem ${context.self.path}"
            stay using TrivialFileSystemData(path)
        }

        //Mensajes de usuario
        case SysCall(m) => m match {

          //Retorna una implementación interna de archivo
          case ReadFile(path, filename) =>
            val fullpath = s"${previousState.path}/$path$filename"
            logger ! s"Filesystem: reading $fullpath"
            try {
              stay using previousState replying File(
                path,
                filename,
                Source.fromFile(fullpath).getLines().toList
              )
            }
            catch {
              case e:FileNotFoundException =>
                logger ! s"FILESYSTEM: file not found $fullpath"
                stay()
            }

        }
      }
  }
}

//Estado
case class TrivialFileSystemData(path:String) extends Data

//System Calls del Filesystem
case class ReadFile(path:String, fileName:String) extends SystemCallType
case class Alive() extends SystemCallType

//Mensajes internos de implementación
case class InitPath(path:String) extends InternalImplementationMessageType