package unq.so.tp.actors.sistema

case class PCB(pid:Int, archivo:File, state:PCBState) {
  var ip: Int = 0

  //Esto fue un paso intermedio, ya no se usa, ahora se busca en la memoria
  def nextInstruction = archivo.content.drop(ip).head

  def hasNext = archivo.content.length > ip
  def dataFirstPos = archivo.content.length
}

trait PCBState
case object Ready extends PCBState
case object IOState extends PCBState
case object ProccesingState extends PCBState