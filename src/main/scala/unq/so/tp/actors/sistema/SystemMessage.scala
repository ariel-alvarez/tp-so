package unq.so.tp.actors.sistema

trait SystemMessage {

}

//mensajes a nivel usuario
case class SysCall(id:SystemCallType) extends SystemMessage
trait SystemCallType

//mensajes a nivel SO
case class IRQ(id:InterruptionType) extends SystemMessage
trait InterruptionType

//mensajes a nivel implementación
case class IIM(id:InternalImplementationMessageType) extends SystemMessage
trait InternalImplementationMessageType

case object DescribeState extends InternalImplementationMessageType
case object Initialize extends InternalImplementationMessageType