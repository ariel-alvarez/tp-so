package unq.so.tp.actors.sistema.memory

import unq.so.tp.actors.SOActor
import unq.so.tp.actors.fsmpersistence.InitializedState
import unq.so.tp.actors.fsmpersistence.{Data, SOState, UninitializedData, UninitializedState}
import unq.so.tp.actors.sistema.{PCB, InternalImplementationMessageType, IIM}
import unq.so.tp.actors.dispositivos.{MoveSequence, WriteSequence}

case class ContiguousAssignment(memSize:Int, dataSizeSpace:Int) extends SOActor with MemoryStrategy{
  //PCB_ID -> (pos_ini, pos_fin)
  startWith( InitializedState, ProcessMap( Seq[Block](Block(None, 0, memSize)) )  )

  when(InitializedState){
    case Event(IIM(callType), ProcessMap(mapa)) => callType match {


      case Translate(relative, pcbId) =>
        mapa.find(_.pcbId == Some(pcbId) ) match {
          case None => throw new Exception(s"El PCB con id=$pcbId no existe")
          case Some(b) if relative >= b.size=> throw new Exception(s"SEGMENTATION FAULT!!!! ini=${b.ini}+relative=$relative >  ${b.size}  | PCB(id=$pcbId)")
          case Some(b) => stay using ProcessMap(mapa) replying( b.ini + relative)
        }



      case Allocate(pcb) =>
        val content = pcb.archivo.content ++ List.fill(dataSizeSpace)("")
        var memMap:Seq[Block] = mapa

        //Si la suma total de espacios es menor a lo required, no hay espacio suficiente
        if (mapa.filter(!_.pcbId.isDefined).map(_.size).sum < content.size)
          throw new Exception(s"MEMORY IS FULL!!!! Cannot allocate $pcb")

        //Si no hay bloque libre de tamaño mayor al required, hay que compactar
        if(!mapa.exists( (b)=> !b.pcbId.isDefined && b.size >= content.size) ){
          memMap = compactUntilSatisfy(mapa, content.size)
        }

        //Buscar bloque a rellenar usando best fit
        val bloqueLibre = findFreeBlock(memMap, content, pcb)

        val newBlock = Block(Some(pcb.pid), bloqueLibre.ini, bloqueLibre.ini+content.size)
        val emptyBlock = Block(None, bloqueLibre.ini+content.size, bloqueLibre.fin)
        val newMap = (memMap diff List(bloqueLibre)) :+ newBlock

        findMemory() ! IIM(WriteSequence(newBlock.ini, content))
        logger ! s"ALLOCATED: ${if (emptyBlock.size > 0) newMap :+ emptyBlock else newMap}"
        stay using ProcessMap(if (emptyBlock.size > 0) newMap :+ emptyBlock else newMap )



      case Free(pcb) =>
        logger ! s"LIBERATING MEMORY: $pcb"
        mapa.find(_.pcbId == Some(pcb.pid)) match {
          case None => stay() //No importa
          case Some(b) =>
            val sorted: Seq[Block] = ((mapa diff Seq(b)) :+ Block(None, b.ini, b.fin)).sortBy(_.ini)

            stay using ProcessMap(
              mergeContiguousHoles(sorted)
            )
        }


    }
  }


  def compactUntilSatisfy(mapa: Seq[Block], required: Int): Seq[Block] = {
    val sorted: Seq[Block] = mergeContiguousHoles(mapa).sortBy(_.ini)

    sorted.tail.foldLeft( (0, sorted.head +: Seq.empty[Block]) )(
      (acum, item) => (acum, item )match {

        case ( (acumSize, acumBlock) , aBlock ) if acumSize >= required => (acumSize, aBlock +: acumBlock)

        case ((acumSize, emptyBlock@Block(None, ini, fin)::xs), (usedBlock@Block(Some(pcb), ini2, fin2)))  =>
          findMemory() ! IIM(MoveSequence(ini2, fin2, ini))
          (emptyBlock.size + acumSize, Block(Some(pcb), ini, ini+usedBlock.size) +: xs)

        case ( (acumSize, acumBlock) , aBlock ) => (acumSize, aBlock +: acumBlock)

      }
    )._2
  }

  def findFreeBlock(mapa: Seq[Block], content: Seq[String], pcb: PCB): Block = {
      bestFit(mapa, content.size) match {
        case Some(blck) => blck
        case None => throw new Exception(s"UNEXPECTED ERROR: cannot fit pcb $pcb")
      }
  }

  def mergeContiguousHoles(sorted: Seq[Block]): Seq[Block] = {
    sorted.tail.foldLeft(Seq.empty[Block] :+ sorted.head)(
      (acum: Seq[Block], item: Block) => (acum.head, item) match {
        case (Block(None, _, _), Block(None, _, _)) =>
          Block(None, acum.head.ini.min(item.ini), acum.head.fin.max(item.fin)) +: acum.tail
        case _ => item +: acum
      }
    )
  }

  //DEJO ESTE CÓDIGO REPETIDO PARA MOSTRAR REFACTORS EN LA PRESENTACIÓN
  def bestFit(mem:Seq[Block], sizeToFit:Int): Option[Block] = {
    mem.filter( (block:Block) => !block.pcbId.isDefined && block.size >= sizeToFit ).sortBy(_.size) match {
      case Nil => None
      case x::xs => Some(x)
    }
  }

  def worstFit(mem:Seq[Block], sizeToFit:Int): Option[Block] = {
    mem.filter( (block:Block) => !block.pcbId.isDefined && block.size >= sizeToFit ).sortBy(-_.size) match {
      case Nil => None
      case x::xs => Some(x)
    }
  }

  def firstFit(mem:Seq[Block], sizeToFit:Int): Option[Block] = {
    mem.filter( (block:Block) => !block.pcbId.isDefined && block.size >= sizeToFit ).sortBy(_.ini) match {
      case Nil => None
      case x::xs => Some(x)
    }
  }

}

//DATA
case class ProcessMap(mapa:Seq[Block]) extends Data
case class Block(pcbId:Option[Int], ini:Int, fin:Int){
  def size:Int = fin-ini
}

//IIM
case class Translate(relativePos:Int, pcbId:Int) extends InternalImplementationMessageType
case class Allocate(pcb:PCB) extends InternalImplementationMessageType
case class Free(pcb:PCB) extends InternalImplementationMessageType