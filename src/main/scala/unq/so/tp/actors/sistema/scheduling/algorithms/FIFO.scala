package unq.so.tp.actors.sistema.scheduling.algorithms

import unq.so.tp.actors.sistema.scheduling.ProcessWrapper

case class FIFO() extends SchedulingAlgorithm {
  override def schedule(current: List[ProcessWrapper]): List[ProcessWrapper] = current

  override def initialize(incoming: ProcessWrapper): ProcessWrapper = incoming
}
