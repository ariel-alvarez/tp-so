package unq.so.tp.actors.sistema

import akka.actor.{Actor, FSM}
import unq.so.tp.actors.fsmpersistence.{Data, InitializedState, SOState}
import unq.so.tp.actors.logging.Loggable
import unq.so.tp.actors.soframework.FiveTimeout
import unq.so.tp.actors.SOActor
import unq.so.tp.actors.sistema.memory.Allocate

class PCBHome  extends SOActor with FiveTimeout {
  startWith(InitializedState, PCBContainer(0, Seq.empty[PCB]))

  when(InitializedState) {
    case Event(event, previousState@PCBContainer(lastPID, pcbList)) =>
      event match {

        //Mensajes de la implementación
        case m: IIM => m match {

          case IIM(LoadProgram(archivo : File)) =>

            val newPID = lastPID + 1
            val newPCB = PCB(newPID, archivo, Ready)
            findMemStrategy() ! IIM(Allocate(newPCB))

            logger ! s"PCB Ready: ${newPCB.toString}"
            stay using PCBContainer(newPID, pcbList :+ newPCB) replying newPCB

          case IIM(GetPCB(pid:Int)) =>
            stay using previousState replying pcbList.find(_.pid == pid)

        }
      }
  }
}

case class PCBContainer(lastPID:Int, pcbs:Seq[PCB]) extends Data

//Internal Implementation Messages
case class LoadProgram(a:File) extends InternalImplementationMessageType
case class GetPCB(pid:Int) extends InternalImplementationMessageType
