package unq.so.tp.actors.sistema

case class File(path: String, name: String, content:Seq[String]) {
  def fullName = s"$path/$name"
}
