package unq.so.tp.actors.sistema.scheduling.algorithms

import unq.so.tp.actors.sistema.scheduling.ProcessWrapper

case class Priority() extends SchedulingAlgorithm {
  override def schedule(current: List[ProcessWrapper]): List[ProcessWrapper] = current sortBy(_.priority)

  override def initialize(incoming: ProcessWrapper): ProcessWrapper =
    ProcessWrapper(incoming.pcb, incoming.timeout, incoming.remainingCycles, incoming.priority)


}
