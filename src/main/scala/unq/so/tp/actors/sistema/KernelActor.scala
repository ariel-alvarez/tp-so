package unq.so.tp.actors.sistema

import akka.actor.{Actor, ActorRef, FSM, Props}
import akka.pattern.ask
import unq.so.tp.actors.fsmpersistence._
import unq.so.tp.actors.logging.Loggable
import unq.so.tp.actors.sistema.filesystem.ReadFile
import unq.so.tp.actors.soframework.{DummyActor, FiveTimeout}
import unq.so.tp.actors.SOActor
import unq.so.tp.actors.sistema.scheduling.ProcessReady

class KernelActor extends SOActor  with FiveTimeout  {

  startWith(InitializedState, KernelData(context.actorOf(Props[DummyActor] ), context.actorOf(Props[DummyActor] )))
  val pcbHome = context.actorOf(Props[PCBHome], "PCBHome")

  when(InitializedState) {
    case Event(event, previousState@KernelData(scheduler,filesystem)) =>
       event match {

         //Mensajes del SO
         case IRQ(e) => e match {
           case Load(archivo) =>
             logger ! s"Kernel: loading file ${archivo.fullName}"
             stay using previousState
         }

         //Syscalls
         case SysCall(s) => s match {
           case Exec(path, filename, priority) =>
             logger ! s"Kernel: executing file $filename"
             filesystem ? SysCall(ReadFile(path, filename))  onSuccess {
               case a:File => pcbHome ? IIM(LoadProgram(a)) onSuccess {
                 case pcb:PCB => scheduler ! IRQ(ProcessReady(pcb, priority))
               }
             }
             stay using previousState
         }

         //Mensajes de la implementación
         case IIM(message) => message match {

           case InitScheduler(newScheduler) =>
             logger ! s"Kernel: loading new scheduler"
             newScheduler ! IIM(Initialize)
             stay using KernelData(newScheduler, filesystem)

           case InitFilesystem(newFilesystem:ActorRef) =>
             newFilesystem ! IIM(Initialize)
             logger ! s"Kernel: loading new filesystem"
             stay using KernelData(scheduler, newFilesystem)

         }
       }
  }
}

//Interrution Requests
case class Load(file:File) extends InterruptionType
case class KernelData(scheduler:ActorRef, filesystem:ActorRef) extends Data

//Syscalls
case class Exec(path:String,filename:String, priority:Int) extends SystemCallType

//Internal Implementation Messages
case class InitScheduler(newScheduler:ActorRef) extends InternalImplementationMessageType
case class InitFilesystem(newFilesystem:ActorRef) extends InternalImplementationMessageType