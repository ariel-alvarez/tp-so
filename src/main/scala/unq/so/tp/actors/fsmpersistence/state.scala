package unq.so.tp.actors.fsmpersistence


// States of FSM
trait SOState

//data for a given state
trait Data
case object UninitializedData extends Data
// States of FSM
case object UninitializedState extends SOState
case object InitializedState extends SOState