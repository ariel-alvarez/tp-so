package unq.so.tp.actors.soframework
import akka.util.Timeout

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

trait FiveTimeout {
  implicit val timeout = Timeout(5 seconds)
  implicit val ec = ExecutionContext.Implicits.global
}
