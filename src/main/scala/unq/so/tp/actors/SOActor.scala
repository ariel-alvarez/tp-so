package unq.so.tp.actors

import akka.actor.{ActorSelection, FSM, Actor}
import unq.so.tp.actors.fsmpersistence.{Data, SOState}
import unq.so.tp.actors.logging.Loggable
import unq.so.tp.actors.sistema.{DescribeState, IIM}
import unq.so.tp.actors.sistema.scheduling.ProcessWrapper
import scala.concurrent.Await
import unq.so.tp.actors.sistema.memory.Translate
import akka.util.Timeout

class SOActor extends Actor with FSM[SOState, Data] with Loggable {
  def findScheduler(): ActorSelection = context.actorSelection("akka://default/user/manager/booter/scheduler-*")
  def findCPUs(): ActorSelection = context.actorSelection("akka://default/user/manager/booter/cpu-*")
  def findKernel(): ActorSelection = context.actorSelection("akka://default/user/manager/booter/kernel-*")
  def findMemory(): ActorSelection = context.actorSelection("akka://default/user/manager/booter/memoria-*")
  def findMemStrategy(): ActorSelection = context.actorSelection("akka://default/user/manager/booter/memstrategy-*")

  whenUnhandled {
    case Event(DescribeState, data) =>
      logger ! s"STATE: $data"
      stay()

    case event =>
      logger ! s"UNHANDLED ${this.self.path} recibió ${event.toString}"
      stay()
  }
}
