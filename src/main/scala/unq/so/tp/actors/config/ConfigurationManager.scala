package unq.so.tp.actors.config

import akka.actor.Actor

class ConfigurationManager extends Actor {
  override def receive: Actor.Receive = {
    case FindConfiguration(id) => sender ! ConfigurationContainer.configuraciones.get(ID(id))
    case _ =>
  }
}


//Mensajes
case class FindConfiguration(id:String)