package unq.so.tp.actors.config

import akka.actor._
import unq.so.tp.actors.sistema._
import unq.so.tp.actors.sistema.filesystem.TrivialFilesystemActor
import unq.so.tp.actors.sistema.scheduling.algorithms.SchedulingAlgorithm
import unq.so.tp.actors.sistema.IIM
import unq.so.tp.actors.sistema.scheduling.algorithms.Priority
import unq.so.tp.actors.sistema.scheduling.algorithms.RoundRobin
import unq.so.tp.actors.sistema.scheduling.SchedulerActor
import unq.so.tp.actors.sistema.InitFilesystem
import unq.so.tp.actors.sistema.memory.ContiguousAssignment
import unq.so.tp.actors.dispositivos.MemoryActor
import unq.so.tp.actors.dispositivos.CpuActor
import unq.so.tp.actors.dispositivos.InputOuputActor
import unq.so.tp.actors.sistema.InitScheduler
import unq.so.tp.actors.logging.Logger

object ConfigurationContainer {
  val configuraciones : Map[ID,Configuration] = Map(



    ID("Config1") -> Configuration(
      ID("Config1"),
      Kernel(
        ID("kernel1"),
        filesystem = TrivialFileSystem(
          ID("TrivialFilesystemUnico"),
          path = "/home/logain/Experimentos/tpso/mounted/discoA"
        ),
        scheduler = Scheduler(
          ID("Scheduler1"),
          RoundRobin( timeout = 2 ),
          Priority()
        ),
        ContiguousMemoryAllocation( ID("AsignacionContigua"), tam=16 )
      ),
      CPU(
        ID("ID1"),
        cycleDurationInSeconds  = 1
      ),
      Disc(
        ID("ID_Disco1")
      ),
      Memory(
        ID("ID_Memoria1"),
        tam = 64
      ),
      IODevice(
        ID("printer")
      )
    ),




    ID("Config2") -> Configuration(
      ID("Config2"),
      Kernel(
        ID("kernel"),
        filesystem = TrivialFileSystem(
          ID("TrivialFilesystemUnico"),
          path = "/home/logain/Experimentos/tpso/mounted/discoB"
        ),
        scheduler = Scheduler(
          ID("Scheduler"),
          RoundRobin( timeout = 2 ),
          Priority()
        ),
        ContiguousMemoryAllocation( ID("AsignacionContigua"), tam=12 , dataSizeSpace = 4)
      ),
      CPU(
        ID("cpu1"),
        cycleDurationInSeconds  = 1
      ),
      Disc(
        ID("ID_Disco1")
      ),
      Memory(
        ID("ID_Memoria1"),
        tam = 64
      ),
      IODevice(
        ID("printer")
      )
    )




  )
}

sealed trait ConfigElement{
  val id: ID
  def apply(context: ActorContext): Seq[ActorRef] = {
    Seq.empty[ActorRef]
  }
}

sealed trait MemStrategy extends ConfigElement
case class ContiguousMemoryAllocation(id:ID, tam: Int, dataSizeSpace:Int=4) extends MemStrategy {
  override def apply(context: ActorContext) : Seq[ActorRef] = {
    Seq(context actorOf (Props(ContiguousAssignment(tam,dataSizeSpace)), "memstrategy-"+id))
  }
}

sealed trait FileSystem extends ConfigElement {
  def createWith(context: ActorContext) : ActorRef
}

case class TrivialFileSystem(id:ID, path:String) extends FileSystem {
  override def createWith(context: ActorContext) : ActorRef = {
    context.actorOf(Props(new TrivialFilesystemActor(path)), "filesystem-" + id.value)
  }
}

case class Scheduler(id:ID, estrategias:SchedulingAlgorithm*) extends ConfigElement

case class CPU(id:ID, cycleDurationInSeconds:Int) extends ConfigElement {
  override def apply(context: ActorContext) : Seq[ActorRef] = {
    Seq(context actorOf(Props(CpuActor(1, cycleDurationInSeconds)), "cpu-" + id.value))
  }
}

case class Disc(id:ID) extends ConfigElement
case class IODevice(id: ID) extends ConfigElement {
  override def apply(context: ActorContext): Seq[ActorRef] = Seq(context actorOf(Props(InputOuputActor(2000)), "iodevice-" + id.value))
}

case class Memory(id:ID, tam:Int) extends ConfigElement {
  override def apply(context: ActorContext): Seq[ActorRef] = Seq(context actorOf(Props(MemoryActor(tam)), "memoria-" + id.value))
}

case class Configuration(id: ID, kernel: Kernel, elements: ConfigElement*) extends ConfigElement {
  override def apply(context: ActorContext) : Seq[ActorRef] = {
    elements.map(e => e.apply(context) ).flatten ++ kernel.apply(context)
  }
}

case class Kernel(
                   id: ID,
                   filesystem: FileSystem,
                   scheduler: Scheduler,
                   memoryStrategy: MemStrategy,
                   args: ConfigElement*) extends ConfigElement{

  override def apply(context: ActorContext) : Seq[ActorRef] = {
    val aKernelRef = context actorOf (Props[KernelActor], "kernel-" + id.value)
    val aSchedulerRef = context actorOf (Props(SchedulerActor(scheduler.estrategias)), "scheduler-" + scheduler.id.value)
    val aFilesystemRef = filesystem createWith context

    memoryStrategy.apply(context)

    aKernelRef ! IIM(InitScheduler(aSchedulerRef))
    aKernelRef ! IIM(InitFilesystem(aFilesystemRef))

    Seq(aKernelRef,aSchedulerRef,aFilesystemRef)
  }

}

case class ID(value:String){
  override def toString = value
}