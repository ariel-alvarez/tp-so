package unq.so.tp.actors.dispositivos.instructions

import sun.reflect.generics.reflectiveObjects.NotImplementedException
import unq.so.tp.actors.dispositivos.IOCommand
import unq.so.tp.actors.fsmpersistence.{Data, SOState}
import akka.actor.FSM
import unq.so.tp.actors.sistema.scheduling.ProcessWrapper

trait InstructionSet {
  def instructionOn(pcb:ProcessWrapper, deviceId:String, command:IOCommand):FSM.State[SOState, Data] = throw new NotImplementedException
  def instructionNop(pcb:ProcessWrapper):FSM.State[SOState, Data] = throw new NotImplementedException
  def instructionOp(pcb:ProcessWrapper, content:String, arguments:List[String]):FSM.State[SOState, Data] = throw new NotImplementedException
  def instructionLet(pcb:ProcessWrapper, pos:Int, content:String):FSM.State[SOState, Data] = throw new NotImplementedException
}