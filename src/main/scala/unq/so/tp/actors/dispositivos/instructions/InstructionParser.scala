package unq.so.tp.actors.dispositivos.instructions

import unq.so.tp.actors.dispositivos.IOCommand
import unq.so.tp.actors.fsmpersistence.{Data, SOState}
import akka.actor.FSM
import unq.so.tp.actors.sistema.scheduling.ProcessWrapper

case class InstructionParser(block: String, instructionSet: InstructionSet, wrapper: ProcessWrapper) {

  def parse(): Instruction = {
    block match {

      case str if str.startsWith("ON") => On(str.split(" ").tail.toList)
      case str if str.startsWith("OP") => Op(str.split(" ").tail.toList)
      case str if str.startsWith("LET") => Let(str.split(" ").tail.toList)

      case _ => Nop
    }
  }
}

case object Nop extends Instruction {
  override def execute(instructionSet: InstructionSet, schedulingStr: ProcessWrapper): FSM.State[SOState, Data] = instructionSet.instructionNop(schedulingStr)
}

case class On(operation: List[String]) extends Instruction {
  val deviceId: String = operation.head
  val deviceOperation = operation.tail.head
  val operationArguments = operation.tail.tail.toList

  override def execute(instructionSet: InstructionSet, wrapper: ProcessWrapper): FSM.State[SOState, Data] = {
    instructionSet.instructionOn(wrapper, deviceId,
      IOCommand(deviceOperation, operationArguments)
    )
  }
}

case class Op(operation: List[String]) extends Instruction {
  val cpuOperation: String = operation.head
  val operationArguments = operation.tail

  override def execute(instructionSet: InstructionSet, wrapper: ProcessWrapper): FSM.State[SOState, Data] = {
    instructionSet.instructionOp(wrapper, cpuOperation, operationArguments)
  }
}

case class Let(operation: List[String]) extends Instruction {
  val memoryPosition: Int = operation.head.toInt
  val content = operation.tail.mkString(" ")

  override def execute(instructionSet: InstructionSet, wrapper: ProcessWrapper): FSM.State[SOState, Data] = {
    instructionSet.instructionLet(wrapper, memoryPosition, content)
  }
}




