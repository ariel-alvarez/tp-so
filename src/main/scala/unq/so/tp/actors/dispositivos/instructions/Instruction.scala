package unq.so.tp.actors.dispositivos.instructions

import unq.so.tp.actors.fsmpersistence.{Data, SOState}
import akka.actor.FSM
import unq.so.tp.actors.sistema.scheduling.ProcessWrapper

trait Instruction {
  def execute(instructionSet: InstructionSet, schedulingStr: ProcessWrapper): FSM.State[SOState, Data]
}
