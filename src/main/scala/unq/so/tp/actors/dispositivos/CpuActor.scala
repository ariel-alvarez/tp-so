package unq.so.tp.actors.dispositivos
import unq.so.tp.actors.SOActor
import unq.so.tp.actors.fsmpersistence.{Data, SOState, UninitializedData, UninitializedState}
import unq.so.tp.actors.sistema._
import akka.pattern.ask
import scala.concurrent.duration._
import unq.so.tp.actors.dispositivos.instructions.{Instruction, InstructionSet}
import akka.actor.{ActorRef, FSM, ActorSystem}
import unq.so.tp.actors.sistema.scheduling._
import unq.so.tp.actors.sistema.IIM
import unq.so.tp.actors.sistema.scheduling.CPUInitialized
import unq.so.tp.actors.dispositivos.instructions.InstructionParser
import unq.so.tp.actors.sistema.scheduling.CPUFree
import unq.so.tp.actors.sistema.IRQ
import unq.so.tp.actors.sistema.scheduling.ProcessWrapper
import unq.so.tp.actors.sistema.memory.Translate
import scala.concurrent.Await
import akka.util.Timeout
import sun.reflect.generics.reflectiveObjects.NotImplementedException
import unq.so.tp.actors.logging.Output

case class CpuActor(instructionsPerCycle: Int, cycleDurationInSeconds: Int) extends SOActor with InstructionSet{
  val system = ActorSystem("system")
  implicit val timeout = Timeout(5 seconds)
  import system.dispatcher

  startWith(UninitializedState, UninitializedData)

  when(UninitializedState) {

    case Event(IIM(internalMessage),data) => internalMessage match {
      case  DescribeState =>
        logger ! s"Estado: UninitializedState"
        stay using data

      case Initialize =>
        system.scheduler.schedule(1 seconds, cycleDurationInSeconds seconds, this.self, Tick)
        findScheduler() ! IIM(CPUInitialized(this.self))
        goto(KernelModeState) using Empty
    }

  }

  when(KernelModeState) {

    case Event(IRQ(irq), data) => irq match {
      case RunPCB(schedullingStructure) =>
        logger ! s"CPU: running ${schedullingStructure.toString}"
        goto(UserModeState) using CPUData(schedullingStructure)

      case _ => stay using data

    }

    case Event(IIM(internalMessage), data) => internalMessage match {
      case  DescribeState =>
        logger ! s"Estado: KernelModeState"
        stay using data
    }

    case Event(Tick, data) =>
      //No ejecutar instrucciones si está en modo kernel
      stay using data
  }

  when(UserModeState) {
    case Event(IRQ(irq), CPUData(pcb)) => irq match {
      case _ => goto(KernelModeState) using CPUData(pcb)

    }

    case Event(IIM(internalMessage), data) => internalMessage match {
      case  DescribeState =>
        logger ! s"Estado: UserModeState"
        stay using data
    }

    //Si se ejecutó la última instrucción => liberar
    case Event(Tick, CPUData(schedulingStructure)) if !schedulingStructure.pcb.hasNext =>
      findScheduler() ! IRQ(ProcessFinished(this.self, schedulingStructure))
      goto(KernelModeState) using Empty

    //Si se produjo time out
    case Event(Tick, CPUData(wrapper)) if wrapper.timeout != SchedulingConstants.NO_TIMEOUT && wrapper.remainingCycles == 0 =>
      findScheduler() ! IRQ(TimedOut(wrapper, this.self))
      goto(KernelModeState) using Empty

    case Event(Tick, CPUData(schStructure)) =>


      val wrapper = ProcessWrapper(
        schStructure.pcb,
        schStructure.timeout,
        if (schStructure.timeout != SchedulingConstants.NO_TIMEOUT) schStructure.remainingCycles-1 else schStructure.remainingCycles,
        schStructure.priority
      )

      val instr = getValueFrom(schStructure, schStructure.pcb.ip)

      val instruction: Instruction = InstructionParser(instr, this, wrapper).parse()
      wrapper.pcb.ip += 1
      instruction.execute(this, wrapper)

  }


  def getValueFrom(schStructure: ProcessWrapper, pos:Int):String =  {
    //se obtiene el valor de la posición de memoria traducida
    val memValueFuture = findMemory() ? IIM(Read(translatePosition(schStructure, pos)))

    //Se obtiene el valor de esa posición de memoria
    Await.result(memValueFuture, 5 seconds).asInstanceOf[String]
  }

  def translatePosition(schStructure: ProcessWrapper, pos:Int): Int = {
    //Se pregunta por la dirección absoluta
    val futurePos = Await.result(findMemStrategy().resolveOne(), 5 seconds) ? IIM(Translate(pos, schStructure.pcb.pid))

    //Se obtiene la posición absoluta en memoria
    Await.result(futurePos, 5 seconds).asInstanceOf[Int]
  }

  def deallocateProcess = {
    findScheduler() ! IRQ(CPUFree(this.self))
    goto(KernelModeState) using Empty
  }


  /*******************************************************************************************************************/
  /********************************************  INSTRUCTION SET  ****************************************************/
  /*******************************************************************************************************************/

  override def instructionOn(pcb:ProcessWrapper, deviceId:String, command:IOCommand): FSM.State[SOState, Data] = {
    context.actorSelection(s"akka://default/user/manager/booter/iodevice-$deviceId") ! IRQ(IORequest(pcb, command))
    deallocateProcess
  }

  override def instructionNop(pcb:ProcessWrapper):FSM.State[SOState, Data] = {
    logger ! Output(s":NOP")
    stay using CPUData(pcb)
  }

  override def instructionLet(pcb:ProcessWrapper, pos:Int, content:String):FSM.State[SOState, Data] = {
    findMemory() ? IIM(Write(translatePosition(pcb, pos+pcb.pcb.archivo.content.size), content))
    stay using CPUData(pcb)
  }

  override def instructionOp(pcb:ProcessWrapper, operation:String, arguments:List[String]):FSM.State[SOState, Data] = {
    operation match {
      case "exec" =>
        findKernel() ! SysCall(Exec("/", arguments.head, if(arguments.tail.isEmpty) 9999 else arguments.tail.head.toInt))
        stay using CPUData(pcb)

        //esto no deberia ir aca, pero son las 18:12 y quiero armarlo rápido : D
      case "concat" =>
        val pos1::pos2::target::tail= arguments
        val val1 = getValueFrom(pcb, pos1.toInt+pcb.pcb.dataFirstPos)
        val val2 = getValueFrom(pcb, pos2.toInt+pcb.pcb.dataFirstPos)
        instructionLet(pcb, target.toInt, val1+val2)

      case _ =>
        logger ! s"Ejecutando ==> OP $operation ${arguments.toString()}"
        stay using CPUData(pcb)
    }

  }

}


// FSM STATES
case object UserModeState extends SOState
case object KernelModeState extends SOState

// FSM DATA
case object Empty extends Data
case class CPUData(pcb: ProcessWrapper) extends Data

//IRQs
case class RunPCB(pcb:ProcessWrapper) extends InterruptionType

//IIM
case object Tick extends InternalImplementationMessageType
