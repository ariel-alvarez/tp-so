package unq.so.tp.actors.dispositivos

import unq.so.tp.actors.SOActor
import unq.so.tp.actors.sistema._
import unq.so.tp.actors.sistema.IRQ
import unq.so.tp.actors.fsmpersistence.{Data, SOState}
import akka.actor.{ActorSelection, ActorSystem}
import scala.concurrent.duration._
import unq.so.tp.actors.sistema.scheduling.{IOFinished, ProcessWrapper}
import unq.so.tp.actors.logging.Output
import scala.concurrent.Await
import unq.so.tp.actors.sistema.memory.Translate
import akka.pattern.ask
import akka.util.Timeout

case class InputOuputActor(delayInMilliseconds: Int) extends SOActor {
  implicit val timeout = Timeout(5 seconds)
  val system = ActorSystem("system")
  import system.dispatcher

  startWith(FreeState, IOWaitQueue(List.empty[IORequest]))

  when(FreeState) {

    case Event(IRQ(irq), IOWaitQueue(procesos)) => irq match {
      case req@IORequest(process, command) =>
        logger ! s"IO-DEVICE RECEIVED $command"
        system.scheduler.scheduleOnce(delayInMilliseconds milliseconds, this.self, IIM(ExecutionFinished))
        goto(InUseState) using IOWaitQueue( req :: procesos)
    }

  }

  when(InUseState) {

    case Event(IRQ(irq), IOWaitQueue(procesos)) => irq match {
      case req@IORequest(newProc, command) =>
        stay using IOWaitQueue(procesos.head :: req :: procesos.tail)
    }

    case Event(IIM(iim), IOWaitQueue(procesos)) => iim match {
      case ExecutionFinished =>

        //logger ! Output(procesos.head.command)
        //se obtiene el valor de la posición de memoria traducida
        val posText: Int = procesos.head.command.parameters.head.toInt + procesos.head.process.pcb.dataFirstPos
        val memValueFuture = findMemory() ? IIM(Read(translatePosition(procesos.head.process,posText)))

        //Se obtiene el valor de esa posición de memoria
        val valorAMostrar = Await.result(memValueFuture, 5 seconds).asInstanceOf[String]
        logger ! Output(valorAMostrar)

        findScheduler() ! IRQ(IOFinished(procesos.head.process))

        if (procesos.tail.isEmpty){
          goto (FreeState) using IOWaitQueue(procesos.tail)
        } else {
          system.scheduler.scheduleOnce(delayInMilliseconds milliseconds, this.self, IIM(ExecutionFinished))
          stay using IOWaitQueue(procesos.tail)
        }
    }

  }

  def translatePosition(schStructure: ProcessWrapper, pos:Int): Int = {
    //Se pregunta por la dirección absoluta
    val futurePos = Await.result(findMemStrategy().resolveOne(), 5 seconds) ? IIM(Translate(pos, schStructure.pcb.pid))

    //Se obtiene la posición absoluta en memoria
    Await.result(futurePos, 5 seconds).asInstanceOf[Int]
  }

}

//STATES
case object InUseState extends SOState
case object FreeState extends SOState

//DATA
case class IOWaitQueue(processes:List[IORequest]) extends Data

//IRQs
case class IORequest(process:ProcessWrapper,command:IOCommand) extends InterruptionType

//INTERNAL MESSAGE IMPLEMENTATIONS
case object ExecutionFinished extends InternalImplementationMessageType
case class  IOCommand(command:String, parameters:List[String])