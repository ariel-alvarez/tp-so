package unq.so.tp.actors.dispositivos

import akka.actor.{FSM, Actor}
import unq.so.tp.actors.dispositivos.KernelModeState
import unq.so.tp.actors.fsmpersistence._
import unq.so.tp.actors.logging.Loggable
import unq.so.tp.actors.sistema._
import unq.so.tp.actors.dispositivos.Read
import unq.so.tp.actors.dispositivos.Write
import unq.so.tp.actors.SOActor

case class MemoryActor(size:Int) extends SOActor {

  startWith(InitializedState, MemoryMap(Vector[String]()))

  when(InitializedState){
    case Event(IIM(callType), mmap@MemoryMap(vector)) => callType match {

      case Read(pos) =>
        logger ! s"MEMORY: reading data from $pos"
        stay using mmap replying vector(pos)

      case Write(pos, value) =>
        logger ! s"MEMORY: writing data $value in $pos"
        if(pos > size) throw new Exception(s"Posición $pos inválida. Es más grande que el tamaño de la memoria")
        stay using MemoryMap(vector updated(pos, value))

      case WriteSequence(pos, value) =>
        logger ! s"MEMORY: writing data $value in $pos"
        writeSequence(value, pos, vector)

      case MoveSequence(ini:Int, fin:Int, nueva:Int) =>
        logger ! s"MEMORY: moving data $ini to $nueva"
        writeSequence(vector.slice(ini,fin), nueva, vector)
    }
  }


  def writeSequence(value: Seq[String], pos: Int, vector: Vector[String]): FSM.State[SOState, Data] = {
    val strVector = value.toVector
    if (pos + value.size > size) throw new Exception(s"Posición $pos+${value.size} inválida. Es más grande que el tamaño de la memoria")
    stay using MemoryMap(vector.slice(0, pos) ++ strVector ++ vector.slice(pos + strVector.size, vector.size))
  }
}

case class MemoryMap(content:Vector[String]) extends Data

case class Read(posIni:Int) extends InternalImplementationMessageType
case class Write(pos:Int, value:String) extends InternalImplementationMessageType
case class WriteSequence(pos:Int, value:Seq[String]) extends InternalImplementationMessageType
case class MoveSequence(ini:Int, fin:Int, nueva:Int) extends InternalImplementationMessageType