package unq.so.tp

import akka.actor.{ActorSystem, Props}
import unq.so.tp.actors.terminal.{StartTerminal, TerminalDispatcher}
import unq.so.tp.actors.logging.Logger

object Main {

    val system = ActorSystem()

    def main(args: Array[String]): Unit = run()

    def run() = {
      system.actorOf(Props[Logger], Logger.LOGGER_ID)
      val dispatcher = system.actorOf(Props[TerminalDispatcher], "manager")
      dispatcher ! StartTerminal
    }

}
