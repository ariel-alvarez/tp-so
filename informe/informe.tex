% 'IEEEtran' is the IEEE's template for white papers / technical reports
\documentclass[12pt,compsoc]{IEEEtran}

% UTF-8 para tildes
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{enumitem}

% provide \includegraphics{} to embed images
\usepackage[pdftex]{graphicx}
\graphicspath{{graphics/}} % path to your graphics


\title{Informe TP SO}
\author{Ariel Alvarez\thanks{Julio 17, 2014}}


% Here we go!
\begin{document}

\IEEEtitleabstractindextext{
\begin{abstract}
Informe detallando la arquitectura, especificaciones y problemáticas del trabajo práctico de la cátedra de Sistemas Operativos - UNQ
\end{abstract}

\begin{IEEEkeywords}
Sistemas Operativos, Scala, Akka.
\end{IEEEkeywords}}

\maketitle

\IEEEdisplaynontitleabstractindextext
\IEEEpeerreviewmaketitle


\section{Introducción}
\IEEEPARstart{E}{l} trabajo práctico se desarrolló utilizando el lenguaje Scala y el framework de actores Akka, modelando
cada entidad del sistema operativo y hardware a simular con actores.

\section{tecnologías empleadas}
\subsection{Por qué Scala}


Scala es un lenguaje con tipado estructural y permite programar empleando paradigmas funcional y orientado a objetos.
Al ser fuertemente tipado tiende a ser más sencillo de debuggear y permite varias funcionalidades en los IDEs más populares
(tanto ScalaIDE como IntelliJ), como distintos tipos de refactors y análisis de errores. Por otro lado, es mucuho más versátil
que otros lenguajes fuertemente tipados, gracias a features tales como pattern matching, implicits, traits, etc.

\subsection{Por qué Akka}


Akka es un framework de actores que permite abstraer las entidades que deben procesarse concurrentemente, permitiendo al
programador concentrarse en las interfaces de estas entidades, de forma tal que, en rasgos generales, la arquitectura puede pensarse
en términos de objeto-mensaje como se haría en POO, evitando pensar en problemas que se alejan del dominio tales como hilos,
subprocesos, memoria global, etc.


\section{Arquitectura}
\subsection{Protocolo de Comunicación}

En Akka, los mensajes que reciben los actores son interpretados mediante pattern matching; esto permite llevar a cabo
diferentes comportamientos dependiendo de la estructura del mensaje recibido. A su vez, al mensaje recibido se le pueden
aplicar varios niveles de pattern matching, lo cual fue aprovechado en este trabajo para separar los mensajes en
diferentes capas, formando así un protocolo de comunicación entre las entidades de negocio que permite desacoplar mensajes
 de dominio y mensajes de implementación como se observa en ~\ref{fig:protocolo}

\begin{figure}[ht]
\centering
\includegraphics[width=2.5in]{protocolo}
\caption{Protocolo interno de comunicación entre las diferentes entidades de negocio}
\label{fig:protocolo}
\end{figure}

Donde \emph{SysCall} son todos los mensajes que exporta la API del sistema operativo al usuario (en este caso, a la terminal),
\emph{IRQ} son los mensajes de la simulación, y finalmente \emph{IIM} (Internal Implementation Message) son aquellos mensajes
propios de la implementación que, o bien se alejan del dominio, o pertenecen a la simulación pero se encuentran más allá
del scope del trabajo. A continuación se muestra un ejemplo extraído de CPUActor:

\begin{lstlisting}[frame=single,breaklines=true,tabsize=2]
when(KernelModeState) {

  case Event(IRQ(irq), data) => irq match {
    case RunPCB(schedullingStructure) =>
      goto(UserModeState)
      using CPUData(schedullingStructure)

    case _ => stay using data

  }

  case Event(IIM(internalMessage), data) =>
    internalMessage match {
      case DescribeState =>
        stay using data
    }

  case Event(Tick, data) =>
    //KernelMode: no ejecutar
    stay using data
}
\end{lstlisting}


\subsection{Finite State Machines}
Gran parte de los actores del trabajo extienden de la clase SOActor, que se comporta como una máquina de estado finito.
Esto permite diferenciar el comportamiento de los dispoitivos según el estado en que se encuentren, de manera que, por ejemplo,
el actor CPUActor hará caso omiso a los ticks de reloj mientras se encuentre en KernelModeState.
Y cada vez que reciba un mensaje IRQ, pasará a dicho modo, asegurando así que no se ejecutarán instrucciones de programas mientras
el sistema operativo se encuentre realizando tareas.

\subsection{DSL de configuración}
El trabajo cuenta con un DSL interno que se utiliza para configurar las entidades de dominio. Hace uso de case classes para
alimentar un builder que irá instanciando los actores y seteando sus parámetros siguiendo un patrón composite.

\begin{lstlisting}[frame=single,breaklines=true,tabsize=2]
Configuration(
    ID("Config1"),
    Kernel(
        ID("kernel1"),
        filesystem=TrivialFileSystem(
            ID("TrivialFilesystem"),
            path = "../discoA"
        ),
        scheduler = Scheduler(
            ID("Scheduler1"),
            RoundRobin(
              timeout = 2
            ),
            Priority()
        ),
        ContiguousMemoryAllocation(
          ID("AsignacionContigua"),
          tam=16
        )
    ),
    CPU(
        ID("ID1"),
        cycleDurationInSeconds=1
    ),
    Disc(
        ID("ID_Disco1")
    ),
    Memory(
        ID("ID_Memoria1"),
        tam = 64
    ),
    IODevice(
        ID("printer")
    )
)
\end{lstlisting}


\subsection{Terminal}
El trabajo cuenta con una terminal interactiva que aprovecha el pattern matching de scala para parsear los comandos ingresados.
Esto se lleva a cabo dentro de TerminalDispatcher, y admite los siguientes casos:


\begin{description}[style=multiline,leftmargin=5cm]
\item[startup kernel CONFIG] Inicializa un kernel con una configuración
\item[cat FILE] Muestra el contenido de un archivo
\item[exec FILE PRIORITY] Ejecuta un archivo con una prioridad (opcional)
\item[state ID] Muestra el estado actual de una entidad dado su id.
\end{description}


\subsection{Scheduler}
El scheduler se encuentra implementado como un SOActor que es inicializado con una secuencia de
SchedulingAlgorithm. Cuando recibe un evento como ProcessReady o IOFinished, agrega el PCB en cuestión a la lista
de procesos ready, envolviéndo antes el PCB en un ProcessWrapper que contiene la información necesaria para los algoritmos de scheduling.
Una vez agregado a la lista, se aplican los algoritmos de scheduling, que transforman la lista de la siguiente manera:

\begin{lstlisting}[frame=single,breaklines=true,tabsize=2]
  def applySchedulingStrategy(
    pcbList: List[ProcessWrapper],
    incoming: ProcessWrapper):
    List[ProcessWrapper]
    = {
      schedulingAlgorithms
        .foldLeft(pcbList :+ incoming)(
        (
          acum: List[ProcessWrapper],
          algor: SchedulingAlgorithm
        ) => algor.schedule(acum))
    }
\end{lstlisting}

Donde los algoritmos van a ser, FIFO por default, Priority o Round Robin:

\begin{lstlisting}[frame=single,breaklines=true,tabsize=2]
//Priority
def schedule(
  current: List[ProcessWrapper]
): List[ProcessWrapper] =
  current sortBy(_.priority)
\end{lstlisting}

\begin{lstlisting}[frame=single,breaklines=true,tabsize=2]
//Round Robin
def initialize(
  incoming: ProcessWrapper
): ProcessWrapper =
  ProcessWrapper(
    incoming.pcb,
    timeout,
    timeout,
    incoming.priority
  )
\end{lstlisting}

\subsection{Memory Management}

Se modela por un lado la memoria física mediante un MemoryActor, que entiende los
mensajes de Read y Write, y un manager de memoria que consiste en un MemoryStrategy, habiéndose implementado ContiguousAssignment.
El CPUActor conoce a la estrategia y la utiliza para traducir y reservar espacios de memoria. La estrategia es también la encargada
de guardar un diccionario con las posiciones de memoria que le corresponden a cada proceso.

En el caso de la asignación continua de memoria, el diccionario es en realidad una lista de bloques, algunos de los cuales
pueden tener Some(pcb) o None. Conocer la memoria usada implica filtrar por Some, y obtener lso bloques libres implica hacer
lo mismo por None. Por ejemplo, en la implementación de Best Fit:
\begin{lstlisting}[frame=single,breaklines=true,tabsize=2]
  def bestFit(
    mem:Seq[Block],
    sizeToFit:Int): Option[Block]
    = {
    mem.filter(
      (block:Block) =>
        !block.pcbId.isDefined &&
        block.size >= sizeToFit
    ).sortBy(_.size) match {
        case Nil => None
        case x::xs => Some(x)
    }
  }
\end{lstlisting}

\subsection{CPU}
La CPU se encuenta modelada con un SOActor que puede encontrarse en los estados UserMode o KernelMode.
Posee el siguiente set de instrucciones:

\begin{description}[style=multiline,leftmargin=2cm]
\item[NOP] Instrucción sin efecto por sí misma.
\item[ON] ON device operation parameters. Envía una instrucción a un dispositivo de E/S
\item[OP] OP operacion parametros. Envía una SysCall al sistema operativo.
\item[LET] LET memPos texto. Guarda datos en una posición de memoria relativa al proceso que se encuentra ejecutando.
\end{description}

\subsection{IODevice}

Los dispositivos de E/S encuentras modelados con un SOActor, que entiende requests con operaciones y puede
hacer DMA para acceder a los datos referenciados en la operación.

\subsection{FileSystem}
El filesystem se encuentra implementado con un SOActor y solamente se desarolló una estrategia trivial,
que es sencillamente usar la api de archivos de Scala.

\section{problemáticas}

\subsection{Callback Hell}

Un problema que se presentó y debió resolverse con un cambio de arquitectura fue el llamado \emph{Callback Hell}.
Esto es, la dependencia constante de una respuesta por parte de las entidades concurrentes. Esto se resolvió de dos
maneras, por un lado haciendo uso de los Futures que provee Akka, y por el otro modificando la forma en que se
comunicaban los actores, de manera tal que no exista una dependencia con las respuestas a lso eventos, salvo casos muy específicos, como son:
la traducción de memoria virtual a física y la creación de un pcb.

\subsection{Concurrencia con más de una CPU}

En principio, el trabajo admite más de una CPu procesando programas, pero no puede asegurar la estabilidad del sistema en determinados casos.
Por ejemplo, si un CPU pretende modificar memoria mientras otro se encuentra realizando un compactamiento de bloques.
Una manera de reducir este problema fue instanciando un único manager de memoria, con lo cual la modificación
de memoria desde el manager se vuelve secuencial, porque el proceso de mensajes dentor de cada actor es atómico.
Esto, sin embargo, deja sin resolver el caso en el que la compactación se produce en simultáneo con un Write.
Una manera de resolverlo sería separar el manager de la estrategia de memoria, y hacer que el CPu solamente pueda modificar
memoria a traves del manager, de esta manera el acceso a memoria siempre es secuencial, eliminando cualquier sector crítico.
Una solución más eficiente (y a la vez más compleja), sería identificar las instrucciones críticas y
bloquear sólo cuando fuera necesario. Por ejemplo, no bloquear si ambas CPUs estaban intentando hacer una traducción simultánea.

\section{Conclusión}
El uso de Scala y Akka facilitó la creación de entidades de negocio, proveyendo una abstracción rica que permitió
una arquitectura pensada en términos de objetos y mensajes. Sin embargo, también generó cierto overhead a la hora de hacer
que estas entidades interactúen, pero pudo ser resuelto en su mayor parte tomando decisiones arquitecturales. Cabe destacar que
al iniciar este trabajo el autor no contaba con extensos conocimientos en Akka (ni pretende tenerlos ahora), con lo cual significó al mismo
tiempo un aprendizaje del paradigma de actores y sistemas concurrentes además de conceptos de sistemas operativos.

\end{document}
