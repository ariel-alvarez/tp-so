import unq.so.tp.actors.config._
import unq.so.tp.actors.sistema.scheduling.algorithms.{Priority, RoundRobin}

Configuration(
    ID("Config1"),
    Kernel(
        ID("kernel1"),
        filesystem = TrivialFileSystem(
            ID("TrivialFilesystemUnico"),
            path = "../discoA"
        ),
        scheduler = Scheduler(
            ID("Scheduler1"),
            RoundRobin( timeout = 2 ),
            Priority()
        ),
        ContiguousMemoryAllocation(
          ID("AsignacionContigua"),
          tam=16
        )
    ),
    CPU(
        ID("ID1"),
        cycleDurationInSeconds  = 1
    ),
    Disc(
        ID("ID_Disco1")
    ),
    Memory(
        ID("ID_Memoria1"),
        tam = 64
    ),
    IODevice(
        ID("printer")
    )
)